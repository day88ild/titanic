import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df["prefix"] = df["Name"].apply(lambda x: "Mr." if "Mr." in x else "Mrs." if "Mrs." in x else "Miss." if "Miss." in x else "None")

    y = round(df["Age"][df["prefix"] == "Mr."].median())
    m = round(df["Age"][df["prefix"] == "Mrs."].median())
    n = round(df["Age"][df["prefix"] == "Miss."].median())

    x = round(df["Age"][df["prefix"] == "Mr."].isna().sum())
    k = round(df["Age"][df["prefix"] == "Mrs."].isna().sum())
    l = round(df["Age"][df["prefix"] == "Miss."].isna().sum())

    df.loc[(df["prefix"] == "Mr.") & df["Age"].isna(), "Age"] = y
    df.loc[(df["prefix"] == "Mrs.") & df["Age"].isna(), "Age"] = m
    df.loc[(df["prefix"] == "Miss.") & df["Age"].isna(), "Age"] = n

    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    return [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
